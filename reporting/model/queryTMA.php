<?php


    $pdo = new PDO('mysql:host=localhost;dbname=optimis', 'denis', '0000');
    $statement = $pdo->query("SELECT organization.name, SUM(stock.quantite) as qte, month(stock.created) as month, year(stock.created) as year
    FROM `ost_stock_tickets` as stock
    LEFT JOIN ost_organization as organization
    ON stock.org_id = organization.id
    WHERE quantite<0
    AND stock.created
    GROUP BY month(stock.created), organization.name");
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);
    header("content-type:application/json");
    echo json_encode($row);
    exit();
