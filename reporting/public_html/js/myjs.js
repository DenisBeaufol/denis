$(document).ready(function () {

    $(".monBouton").click(function () {
        $.ajax({
            url: '../model/queryTMA.php', // La ressource ciblée
            type: 'GET', // Le type de la requête HTTP.
            dataType: 'json', // Le type de données à recevoir
            success: function (data, statut) { // success est toujours en place, bien sûr !
               if (data) {
                    //chart_data = $.parseJSON(data);
                    datasToDisplay(data);
                    //colorRandom();
               }
                //console.log(data, statut);
            },
            error: function (resultat, statut, erreur) {
                console.log(resultat, statut, erreur);
            }
        });
    });
});

function datasToDisplay(data){
    var theDatas = [];
    var MonthPerSoc = [];
    var Societes = [];
    var Color = {};
    data.forEach(function(obj){
      //if(obj.year == "2017"){
        if(Societes.indexOf(obj.name)==-1){
          Societes.push(obj.name);
      //}
      }
    })
    for (var i = 1; i <= 12; i++) {
      var tempMonth = [];
      Societes.forEach(function(societe){
          tempMonth.push("0");
          data.forEach(function(obj){
            if((obj.month == i.toString())&&(obj.name == societe)){
              if (obj.qte != null) {
                tempMonth[(tempMonth.length - 1)] = obj.qte;
              }
            };
          })
      })
      MonthPerSoc.push(tempMonth);
    }
    for (var i = 1; i <= Societes.length; i++){
      Color[Societes[i-1]] = colorRandom();
    }
    console.log(Color);

    theDatas.push(["mois", "qte TMA",{role: 'style'},{role: 'annotationText'}])

    data.forEach(function(obj){
        theDatas.push([obj.month, -1*parseFloat(obj.qte),Color[obj.name],obj.name])
        //theDatas.push([obj.month, -1*parseFloat(obj.qte),obj.name,Color[obj.name]])
    })
    console.log(theDatas);
    drawChart(theDatas);
}
function colorRandom(){

  var random1 = Math.floor((Math.random() * 250) + 1);
  var random2 = Math.floor((Math.random() * 250) + 1);
  var random3 = Math.floor((Math.random() * 250) + 1);
  return "#"+(random1).toString(16)+(random2).toString(16)+(random3).toString(16);
  //rgb(random1,random2,random3);
  //console.log(random1,random2,random3);

}
